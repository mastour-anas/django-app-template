# django-app-template

[![pipeline status](https://gitlab.com/mastour-anas/django-app-template/badges/master/pipeline.svg)](https://gitlab.com/mastour-anas/django-app-template/commits/master) [![coverage report](https://gitlab.com/mastour-anas/django-app-template/badges/master/coverage.svg)](https://gitlab.com/mastour-anas/django-app-template/commits/master)

django-app-template is a sample application to start developing your web project on [Django Framework](https://docs.djangoproject.com "Django Framework")